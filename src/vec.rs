//! PETSc vectors ([`Vec`] objects) are used to store the field variables in PDE-based (or other) simulations.
//!
//! PETSc C API docs: <https://petsc.org/release/docs/manualpages/Vec/index.html>

use crate::{prelude::*, InsertMode, Int, Real, Scalar};

/// [`Vec`] Type
pub use crate::petsc_raw::VecTypeEnum as VecType;

/// Abstract PETSc vector object
pub struct Vec<'a> {
    pub(crate) ptr: *mut petsc_raw::_p_Vec, /* I could use Vec which is the same thing, but i think using a pointer is more clear */
    _lifetime: PhantomData<&'a ()>,
}

/// A immutable view of a Vec with [`Deref`] to [`ndarray::ArrayView`].
pub struct VecView<'a, 'b> {
    pub(crate) vec: &'b Vec<'a>,
    pub(crate) array: *const Scalar,
    pub(crate) ndarray: ArrayView<'b, Scalar, ndarray::IxDyn>,
}

/// A mutable view of a Vec with [`DerefMut`] to [`ndarray::ArrayViewMut`].
pub struct VecViewMut<'a, 'b> {
    pub(crate) vec: &'b mut Vec<'a>,
    pub(crate) array: *mut Scalar,
    pub(crate) ndarray: ArrayViewMut<'b, Scalar, ndarray::IxDyn>,
}

/// A wrapper around [`Vec`] that is used when the [`Vec`] shouldn't be destroyed.
///
/// Gives mutable access to the underlining [`Vec`].
///
/// For example, it is used with [`DM::local_vector()`](crate::dm::DM::local_vector()).
pub struct BorrowVecMut<'a, 'bv> {
    pub(crate) owned_vec: ManuallyDrop<Vec<'a>>,
    drop_func: Option<Box<dyn FnOnce(&mut Self) + 'bv>>,
    // do we need this phantom data?
    // also should 'bv be used for the closure
    _phantom: PhantomData<&'bv mut Vec<'a>>,
}

/// A wrapper around [`Vec`] that is used when the [`Vec`] shouldn't be destroyed.
///
/// For example, it is used with [`DM::local_vector()`](crate::dm::DM::local_vector()).
pub struct BorrowVec<'a, 'bv> {
    pub(crate) owned_vec: ManuallyDrop<Vec<'a>>,
    drop_func: Option<Box<dyn FnOnce(&mut Self) + 'bv>>,
    // do we need this phantom data?
    // also should 'bv be used for the closure
    _phantom: PhantomData<&'bv Vec<'a>>,
}

impl Drop for BorrowVecMut<'_, '_> {
    fn drop(&mut self) {
        if let Some(f) = self.drop_func.take() {
            f(self)
        }
    }
}

impl Drop for BorrowVec<'_, '_> {
    fn drop(&mut self) {
        if let Some(f) = self.drop_func.take() {
            f(self)
        }
    }
}

use mpi::topology::SimpleCommunicator;
/// Options for controlling a [`Vec`]'s behavior
pub use petsc_raw::VecOption;

impl<'a> Vec<'a> {
    pub(crate) fn new(ptr: *mut petsc_raw::_p_Vec) -> Self {
        Self {
            ptr,
            _lifetime: PhantomData,
        }
    }

    /// Creates an empty vector object. The type can then be set with [`Vec::set_type`], or [`Vec::set_from_options`].
    /// Same as [`Petsc::vec_create`](crate::Petsc::vec_create).
    ///
    /// ```
    /// # use petsc::{mpi::traits::*, prelude::*, Petsc};
    /// # fn main() -> petsc::Result<()> {
    /// let petsc = Petsc::init_no_args()?;
    ///
    /// petsc::Vec::create(petsc.world())?;
    /// # Ok(()) }
    /// ```
    pub fn create(comm: impl Into<crate::CommOpt>) -> crate::Result<Self> {
        let comm = comm.into().as_raw();
        let mut ptr = MaybeUninit::uninit();
        let ierr = unsafe { petsc_raw::VecCreate(comm, ptr.as_mut_ptr()) };
        crate::Petsc::check_error(comm, ierr)?;
        Ok(Vec::new(unsafe { ptr.assume_init() }))
    }

    /// Builds a [`Vec`], for a particular vector implementation.  (given as `&str`).
    pub fn set_type_str(&mut self, vec_type: &str) -> crate::Result<()> {
        let cstring = CString::new(vec_type).expect("`CString::new` failed");
        let ierr = unsafe { petsc_raw::VecSetType(self.ptr, cstring.as_ptr()) };
        self.check_error(ierr)
    }

    /// Builds a [`Vec`], for a particular vector implementation.
    pub fn set_type(&mut self, vec_type: VecType) -> crate::Result<()> {
        let cstring = petsc_raw::VECTYPE_TABLE[vec_type as usize];
        let ierr = unsafe { petsc_raw::VecSetType(self.ptr, cstring.as_ptr() as *const _) };
        self.check_error(ierr)
    }

    /// Creates a new vector of the same type as an existing vector.
    ///
    /// [`duplicate`](Vec::duplicate) DOES NOT COPY the vector entries, but rather
    /// allocates storage for the new vector. Use [`Vec::copy_data_from()`] to copy a vector.
    ///
    /// Note, if you want to duplicate and copy data you should use [`Vec::clone()`].
    pub fn duplicate(&self) -> crate::Result<Vec<'a>> {
        let mut vec2_ptr = MaybeUninit::uninit();
        let ierr = unsafe { petsc_raw::VecDuplicate(self.ptr, vec2_ptr.as_mut_ptr()) };
        self.check_error(ierr)?;
        Ok(Vec::new(unsafe { vec2_ptr.assume_init() }))
    }

    ///  Assembles the vector by calling [`Vec::assembly_begin()`] then [`Vec::assembly_end()`]
    pub fn assemble(&mut self) -> crate::Result<()> {
        self.assembly_begin()?;
        self.assembly_end()
    }

    /// Sets the local and global sizes, and checks to determine compatibility
    ///
    /// The inputs can be `None` to have PETSc decide the size.
    /// `local_size` and `global_size` cannot be both `None`. If one processor calls this with
    /// `global_size` of `None` then all processors must, otherwise the program will hang.
    pub fn set_sizes(
        &mut self,
        local_size: impl Into<Option<usize>>,
        global_size: impl Into<Option<usize>>,
    ) -> crate::Result<()> {
        let local_size = local_size
            .into()
            .map_or(petsc_raw::PETSC_DECIDE_INTEGER, |v| v as Int);
        let global_size = global_size
            .into()
            .map_or(petsc_raw::PETSC_DECIDE_INTEGER, |v| v as Int);
        let ierr = unsafe { petsc_raw::VecSetSizes(self.ptr, local_size, global_size) };
        self.check_error(ierr)
    }

    /// Inserts or adds values into certain locations of a vector.
    ///
    /// `ix` and `v` must be the same length or `set_values` will panic.
    ///
    /// If you call `x.set_option(VecOption::VEC_IGNORE_NEGATIVE_INDICES, true)`, negative indices
    /// may be passed in ix. These rows are simply ignored. This allows easily inserting element
    /// load matrices with homogeneous Dirichlet boundary conditions that you don't want represented
    /// in the vector.
    ///
    /// These values may be cached, so [`Vec::assembly_begin()`] and [`Vec::assembly_end()`] MUST be
    /// called after all calls to [`Vec::set_values()`] have been completed.
    ///
    /// You might find [`Vec::assemble_with()`] or [`Vec::assemble_with_batched()`] more useful and
    /// more idiomatic.
    ///
    /// Parameters.
    ///
    /// * `ix` - indices where to add
    /// * `v` - array of values to be added
    /// * `iora` - Either [`INSERT_VALUES`](InsertMode::INSERT_VALUES) or [`ADD_VALUES`](InsertMode::ADD_VALUES),
    ///   where [`ADD_VALUES`](InsertMode::ADD_VALUES) adds values to any existing entries, and
    ///   [`INSERT_VALUES`](InsertMode::INSERT_VALUES) replaces existing entries with new values.
    ///
    /// # Example
    ///
    /// ```
    /// # use petsc::{mpi::traits::*, prelude::*, Petsc, ErrorKind, Scalar, InsertMode};
    /// # fn main() -> petsc::Result<()> {
    /// # let petsc = Petsc::init_no_args()?;
    /// if petsc.world().size() != 1 {
    ///     // note, cargo wont run tests with mpi so this will never be reached,
    ///     // but this example will only work in a uniprocessor comm world
    ///     Petsc::set_error(
    ///         petsc.world(),
    ///         ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
    ///         "This is a uniprocessor example only!",
    ///     )?;
    /// }
    ///
    /// let mut v = petsc.vec_create()?;
    /// v.set_sizes(None, 10)?; // create vector of size 10
    /// v.set_from_options()?;
    ///
    /// v.set_values(
    ///     &[0, 3, 7, 9],
    ///     &[
    ///         Scalar::from(1.1),
    ///         Scalar::from(2.2),
    ///         Scalar::from(3.3),
    ///         Scalar::from(4.4),
    ///     ],
    ///     InsertMode::INSERT_VALUES,
    /// )?;
    /// // You MUST assemble before you can use
    /// v.assembly_begin()?;
    /// v.assembly_end()?;
    ///
    /// // We do the map in the case that `Scalar` is complex.
    /// assert_eq!(
    ///     v.values(0..10)?,
    ///     [1.1, 0.0, 0.0, 2.2, 0.0, 0.0, 0.0, 3.3, 0.0, 4.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    ///
    /// v.set_values(
    ///     &vec![0, 2, 8, 9],
    ///     &vec![
    ///         Scalar::from(1.0),
    ///         Scalar::from(2.0),
    ///         Scalar::from(3.0),
    ///         Scalar::from(4.0),
    ///     ],
    ///     InsertMode::ADD_VALUES,
    /// )?;
    /// // You MUST assemble before you can use
    /// v.assembly_begin()?;
    /// v.assembly_end()?;
    /// assert_eq!(
    ///     v.values(0..10)?,
    ///     [2.1, 0.0, 2.0, 2.2, 0.0, 0.0, 0.0, 3.3, 3.0, 8.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    /// # Ok(()) }
    /// ```
    pub fn set_values(&mut self, ix: &[Int], v: &[Scalar], iora: InsertMode) -> crate::Result<()> {
        assert!(iora == InsertMode::INSERT_VALUES || iora == InsertMode::ADD_VALUES);
        assert_eq!(ix.len(), v.len());

        let ni = ix.len() as Int;
        let ierr = unsafe {
            petsc_raw::VecSetValues(self.ptr, ni, ix.as_ptr(), v.as_ptr() as *mut _, iora)
        };
        self.check_error(ierr)
    }

    /// Zeros the array of a vector
    ///
    /// ```
    /// # use petsc::prelude::*;
    /// # fn main() -> petsc::Result<()> {
    /// let petsc = petsc::Petsc::init_no_args()?;
    ///
    /// let mut v = petsc.vec_create()?;
    /// v.set_sizes(1, None)?;
    /// v.set_from_options()?;
    /// v.zero_entries()?;
    /// assert_eq!(v.view()?[0], 0.0);
    /// # Ok(()) }
    /// ```
    pub fn zero_entries(&mut self) -> crate::Result<()> {
        let ierr = unsafe { petsc_raw::VecZeroEntries(self.ptr) };
        self.check_error(ierr)
    }

    /// Allows you to give an iter that will be use to make a series of calls to [`Vec::set_values()`].
    /// Then is followed by both [`Vec::assembly_begin()`] and [`Vec::assembly_end()`].
    ///
    /// [`assemble_with()`](Vec::assemble_with()) will short circuit on the first error
    /// from [`Vec::set_values()`], returning it.
    ///
    /// # Example
    ///
    /// ```
    /// # use petsc::{mpi::traits::*, prelude::*, Petsc, ErrorKind, Scalar, InsertMode};
    /// # fn main() -> petsc::Result<()> {
    /// # let petsc = Petsc::init_no_args()?;
    /// if petsc.world().size() != 1 {
    ///     // note, cargo wont run tests with mpi so this will never be reached,
    ///     // but this example will only work in a uniprocessor comm world
    ///     Petsc::set_error(
    ///         petsc.world(),
    ///         ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
    ///         "This is a uniprocessor example only!",
    ///     )?;
    /// }
    ///
    /// let mut v = petsc.vec_create()?;
    /// v.set_sizes(None, 10)?; // create vector of size 10
    /// v.set_from_options()?;
    ///
    /// v.assemble_with(
    ///     [0, 3, 7, 9].iter().cloned().zip([
    ///         Scalar::from(1.1),
    ///         Scalar::from(2.2),
    ///         Scalar::from(3.3),
    ///         Scalar::from(4.4),
    ///     ]),
    ///     InsertMode::INSERT_VALUES,
    /// )?;
    /// // We do the map in the case that `Scalar` is complex.
    /// assert_eq!(
    ///     v.values(0..10)?,
    ///     [1.1, 0.0, 0.0, 2.2, 0.0, 0.0, 0.0, 3.3, 0.0, 4.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    ///
    /// v.assemble_with(
    ///     [
    ///         (0, Scalar::from(1.0)),
    ///         (2, Scalar::from(2.0)),
    ///         (8, Scalar::from(3.0)),
    ///         (9, Scalar::from(4.0)),
    ///     ],
    ///     InsertMode::ADD_VALUES,
    /// )?;
    /// assert_eq!(
    ///     v.values(0..10)?,
    ///     [2.1, 0.0, 2.0, 2.2, 0.0, 0.0, 0.0, 3.3, 3.0, 8.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    /// # Ok(())
    /// # }
    /// ```
    pub fn assemble_with<I>(&mut self, iter_builder: I, iora: InsertMode) -> crate::Result<()>
    where
        I: IntoIterator<Item = (Int, Scalar)>,
    {
        // We don't actually care about the num_inserts value, we just need something that
        // implements `Sum` so we can use the sum method and `()` does not.
        let _num_inserts = iter_builder
            .into_iter()
            .map(|(ix, v)| {
                self.set_values(std::slice::from_ref(&ix), std::slice::from_ref(&v), iora)
                    .map(|_| 1)
            })
            .sum::<crate::Result<Int>>()?;
        // Note, `sum()` will short-circuit the iterator if an error is encountered.

        self.assembly_begin()?;
        self.assembly_end()
    }

    /// Allows you to give an iter that will be use to make a series of calls to [`Vec::set_values()`].
    /// Then is followed by both [`Vec::assembly_begin()`] and [`Vec::assembly_end()`].
    ///
    /// [`assemble_with()`](Vec::assemble_with()) will short circuit on the first error
    /// from [`Vec::set_values()`], returning it.
    ///
    /// # Example
    ///
    /// ```
    /// # use petsc::{mpi::traits::*, prelude::*, Petsc, Scalar, ErrorKind, InsertMode};
    /// # fn main() -> petsc::Result<()> {
    /// # let petsc = Petsc::init_no_args()?;
    /// if petsc.world().size() != 1 {
    ///     // note, cargo wont run tests with mpi so this will never be reached,
    ///     // but this example will only work in a uniprocessor comm world
    ///     Petsc::set_error(
    ///         petsc.world(),
    ///         ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
    ///         "This is a uniprocessor example only!",
    ///     )?;
    /// }
    ///
    /// let mut v = petsc.vec_create()?;
    /// v.set_sizes(None, 10)?; // create vector of size 10
    /// v.set_from_options()?;
    ///
    /// v.assemble_with_batched(
    ///     [
    ///         ([0, 3], [Scalar::from(1.1), Scalar::from(2.2)]),
    ///         ([7, 9], [Scalar::from(3.3), Scalar::from(4.4)]),
    ///     ],
    ///     InsertMode::INSERT_VALUES,
    /// )?;
    /// assert_eq!(
    ///     &v.values(0..10)?[..],
    ///     &[1.1, 0.0, 0.0, 2.2, 0.0, 0.0, 0.0, 3.3, 0.0, 4.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    ///
    /// v.assemble_with_batched(
    ///     Some((
    ///         [0, 2, 8, 9],
    ///         [
    ///             Scalar::from(1.0),
    ///             Scalar::from(2.0),
    ///             Scalar::from(3.0),
    ///             Scalar::from(4.0),
    ///         ],
    ///     )),
    ///     InsertMode::ADD_VALUES,
    /// )?;
    /// assert_eq!(
    ///     &v.values(0..10)?[..],
    ///     &[2.1, 0.0, 2.0, 2.2, 0.0, 0.0, 0.0, 3.3, 3.0, 8.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    /// # Ok(())
    /// # }
    /// ```
    pub fn assemble_with_batched<I, A1, A2>(
        &mut self,
        iter_builder: I,
        iora: InsertMode,
    ) -> crate::Result<()>
    where
        I: IntoIterator<Item = (A1, A2)>,
        A1: AsRef<[Int]>,
        A2: AsRef<[Scalar]>,
    {
        // We don't actually care about the num_inserts value, we just need something that
        // implements `Sum` so we can use the sum method and `()` does not.
        let _num_inserts = iter_builder
            .into_iter()
            .map(|(ix, v)| self.set_values(ix.as_ref(), v.as_ref(), iora).map(|_| 1))
            .sum::<crate::Result<Int>>()?;
        // Note, `sum()` will short-circuit the iterator if an error is encountered.

        self.assembly_begin()?;
        self.assembly_end()
    }

    /// Gets values from certain locations of a vector.
    ///
    /// Currently can only get values on the same processor.
    ///
    /// Most of the time creating a vector view will be more useful: [`Vec::view()`] or [`Vec::view_mut()`].
    ///
    /// # Example
    ///
    /// ```
    /// # use petsc::{mpi::traits::*, prelude::*, Petsc, ErrorKind, Scalar};
    /// # fn main() -> petsc::Result<()> {
    /// # let petsc = Petsc::init_no_args()?;
    /// if petsc.world().size() != 1 {
    ///     // note, cargo wont run tests with mpi so this will never be reached,
    ///     // but this example will only work in a uniprocessor comm world
    ///     Petsc::set_error(
    ///         petsc.world(),
    ///         ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
    ///         "This is a uniprocessor example only!",
    ///     )?;
    /// }
    ///
    /// let mut v = petsc.vec_create()?;
    /// v.set_sizes(None, 10)?; // create vector of size 10
    /// v.set_from_options()?;
    ///
    /// let ix = [0, 2, 7, 9];
    /// v.set_values(
    ///     &ix,
    ///     &[
    ///         Scalar::from(1.1),
    ///         Scalar::from(2.2),
    ///         Scalar::from(3.3),
    ///         Scalar::from(4.4),
    ///     ],
    ///     petsc::InsertMode::INSERT_VALUES,
    /// )?;
    ///
    /// // We do the map in the case that `Scalar` is complex.
    /// assert_eq!(
    ///     v.values(ix)?,
    ///     [1.1, 2.2, 3.3, 4.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    /// assert_eq!(
    ///     v.values(vec![2, 0, 9, 7])?,
    ///     [2.2, 1.1, 4.4, 3.3]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    /// assert_eq!(
    ///     v.values(0..10)?,
    ///     [1.1, 0.0, 2.2, 0.0, 0.0, 0.0, 0.0, 3.3, 0.0, 4.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    /// assert_eq!(
    ///     v.values((0..5).map(|v| v * 2))?,
    ///     [1.1, 2.2, 0.0, 0.0, 0.0]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    /// # Ok(())
    /// # }
    /// ```
    pub fn values<T>(&self, ix: T) -> crate::Result<std::vec::Vec<Scalar>>
    where
        T: IntoIterator<Item = Int>,
    {
        let ix_iter = ix.into_iter();
        let ix_array = ix_iter.collect::<std::vec::Vec<_>>();
        let ni = ix_array.len();
        let mut out_vec = vec![Scalar::default(); ni];

        let ierr = unsafe {
            petsc_raw::VecGetValues(
                self.ptr,
                ni as Int,
                ix_array.as_ptr(),
                out_vec[..].as_mut_ptr() as *mut _,
            )
        };
        self.check_error(ierr)?;

        Ok(out_vec)
    }

    /// Returns the range of indices owned by this processor.
    ///
    /// This method assumes that the vectors are laid
    /// out with the first n1 elements on the first processor, next n2 elements on the second, etc.
    /// For certain parallel layouts this range may not be well defined.
    ///
    /// # Example
    ///
    /// ```
    /// # use petsc::{mpi::traits::*, prelude::*, Scalar};
    /// # fn main() -> petsc::Result<()> {
    /// # let petsc = petsc::Petsc::init_no_args()?;
    /// // note, cargo wont run tests with mpi so this will always be run with
    /// // a single processor, but this example will also work in a multiprocessor
    /// // comm world.
    ///
    /// // We do the map in the case that `Scalar` is complex.
    /// let values = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0]
    ///     .iter()
    ///     .cloned()
    ///     .map(|v| Scalar::from(v))
    ///     .collect::<std::vec::Vec<_>>();
    /// let mut v = petsc.vec_create()?;
    /// v.set_sizes(None, values.len())?;
    /// v.set_from_options()?;
    ///
    /// let vec_ownership_range = v.ownership_range()?;
    /// let vec_ownership_range_usize =
    ///     (vec_ownership_range.start as usize)..(vec_ownership_range.end as usize);
    ///
    /// v.assemble_with(
    ///     vec_ownership_range
    ///         .clone()
    ///         .zip(values[vec_ownership_range_usize.clone()].iter().cloned()),
    ///     petsc::InsertMode::INSERT_VALUES,
    /// )?;
    ///
    /// // or we could do:
    /// let v2 = petsc::Vec::from_slice(petsc.world(), &values[vec_ownership_range_usize.clone()])?;
    ///
    /// let v_view = v.view()?;
    /// let v2_view = v2.view()?;
    /// assert_eq!(
    ///     v_view.as_slice().unwrap(),
    ///     &values[vec_ownership_range_usize.clone()]
    /// );
    /// assert_eq!(v_view.as_slice().unwrap(), v2_view.as_slice().unwrap());
    /// # Ok(())
    /// # }
    /// ```
    pub fn ownership_range(&self) -> crate::Result<std::ops::Range<Int>> {
        let mut low = MaybeUninit::<Int>::uninit();
        let mut high = MaybeUninit::<Int>::uninit();
        let ierr = unsafe {
            petsc_raw::VecGetOwnershipRange(self.ptr, low.as_mut_ptr(), high.as_mut_ptr())
        };
        self.check_error(ierr)?;

        Ok(unsafe { low.assume_init()..high.assume_init() })
    }

    /// Returns the range of indices owned by EACH processor.
    ///
    /// This method assumes that the vectors are laid
    /// out with the first n1 elements on the first processor, next n2 elements on the second, etc.
    /// For certain parallel layouts this range may not be well defined.
    pub fn ownership_ranges(&self) -> crate::Result<std::vec::Vec<std::ops::Range<Int>>> {
        let mut array = MaybeUninit::<*const Int>::uninit();
        let ierr = unsafe { petsc_raw::VecGetOwnershipRanges(self.ptr, array.as_mut_ptr()) };
        self.check_error(ierr)?;

        let mut comm_size = MaybeUninit::uninit();
        let ierr = unsafe { mpi::ffi::MPI_Comm_size(self.comm().as_raw(), comm_size.as_mut_ptr()) };
        self.check_error_mpi(ierr)?;
        let comm_size = unsafe { comm_size.assume_init() };

        // SAFETY: Petsc says it is an array of length size+1
        let slice_from_array =
            unsafe { std::slice::from_raw_parts(array.assume_init(), comm_size as usize + 1) };
        let array_iter = slice_from_array.iter();
        let mut slice_iter_p1 = slice_from_array.iter();
        let _ = slice_iter_p1.next();
        Ok(array_iter.zip(slice_iter_p1).map(|(s, e)| *s..*e).collect())
    }

    /// Copies a vector. self <- x
    ///
    /// For default parallel PETSc vectors, both x and y MUST be distributed in the same manner;
    /// only local copies are done.
    ///
    /// Note, the vector dont need to have the same type and comm because we allow one of the vectors
    /// to be sequential and one to be parallel so long as both have the same local sizes. This is
    /// used in some internal functions in PETSc.
    pub fn copy_data_from(&mut self, x: &Vec) -> crate::Result<()> {
        let ierr = unsafe { petsc_raw::VecCopy(x.ptr, self.ptr) };
        self.check_error(ierr)
    }

    /// Create an immutable view of the this processor's portion of the vector.
    ///
    /// # Implementation Note
    ///
    /// Standard PETSc vectors use contiguous storage so that this routine does not copy the data.
    /// Other vector implementations may require to copy the data, but must such implementations
    /// should cache the contiguous representation so that only one copy is performed when this routine
    /// is called multiple times in sequence.
    ///
    /// # Example
    ///
    /// ```
    /// # use petsc::{mpi::traits::*, prelude::*, Petsc, ErrorKind, Scalar, InsertMode};
    /// # fn main() -> petsc::Result<()> {
    /// # let petsc = Petsc::init_no_args()?;
    /// if petsc.world().size() != 1 {
    ///     // note, cargo wont run tests with mpi so this will never be reached,
    ///     // but this example will only work in a uniprocessor comm world
    ///     Petsc::set_error(
    ///         petsc.world(),
    ///         ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
    ///         "This is a uniprocessor example only!",
    ///     )?;
    /// }
    /// let mut v = petsc.vec_create()?;
    /// v.set_sizes(None, 10)?; // create vector of size 10
    /// v.set_from_options()?;
    ///
    /// v.assemble_with(
    ///     [0, 3, 7, 9].iter().cloned().zip([
    ///         Scalar::from(1.1),
    ///         Scalar::from(2.2),
    ///         Scalar::from(3.3),
    ///         Scalar::from(4.4),
    ///     ]),
    ///     InsertMode::INSERT_VALUES,
    /// )?;
    /// // We do the map in the case that `Scalar` is complex.
    /// assert_eq!(
    ///     v.values(0..10)?,
    ///     [1.1, 0.0, 0.0, 2.2, 0.0, 0.0, 0.0, 3.3, 0.0, 4.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()
    /// );
    ///
    /// {
    ///     let v_view = v.view()?;
    ///     assert_eq!(
    ///         v_view.as_slice().unwrap(),
    ///         &[1.1, 0.0, 0.0, 2.2, 0.0, 0.0, 0.0, 3.3, 0.0, 4.4]
    ///             .iter()
    ///             .cloned()
    ///             .map(|v| Scalar::from(v))
    ///             .collect::<Vec<_>>()
    ///     );
    /// }
    ///
    /// v.assemble_with(
    ///     [
    ///         (0, Scalar::from(1.0)),
    ///         (2, Scalar::from(2.0)),
    ///         (8, Scalar::from(3.0)),
    ///         (9, Scalar::from(4.0)),
    ///     ],
    ///     InsertMode::ADD_VALUES,
    /// )?;
    ///
    /// // It is valid to have multiple immutable views
    /// let v_view = v.view()?;
    /// let v_view2 = v.view()?;
    /// assert_eq!(v_view.as_slice().unwrap(), v_view2.as_slice().unwrap());
    /// assert_eq!(
    ///     v_view2.as_slice().unwrap(),
    ///     &[2.1, 0.0, 2.0, 2.2, 0.0, 0.0, 0.0, 3.3, 3.0, 8.4]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()[..]
    /// );
    /// # Ok(())
    /// # }
    /// ```
    pub fn view<'b>(&'b self) -> crate::Result<VecView<'a, 'b>> {
        VecView::new(self)
    }

    /// Create an mutable view of the this processor's portion of the vector.
    ///
    /// # Implementation Note
    ///
    /// Returns a slice to a contiguous array that contains this processor's portion of the vector data.
    /// For the standard PETSc vectors, [`view_mut()`](Vec::view_mut()) returns a pointer to the local
    /// data array and does not use any copies. If the underlying vector data is not stored in a contiguous
    /// array this routine will copy the data to a contiguous array and return a slice to that. You MUST
    /// drop the returned [`VecViewMut`] when you no longer need access to the slice.
    ///
    /// # Example
    ///
    /// ```
    /// # use petsc::{mpi::traits::*, prelude::*, ErrorKind, Scalar, Petsc};
    /// # fn main() -> petsc::Result<()> {
    /// # let petsc = Petsc::init_no_args()?;
    /// if petsc.world().size() != 1 {
    ///     // note, cargo wont run tests with mpi so this will never be reached,
    ///     // but this example will only work in a uniprocessor comm world
    ///     Petsc::set_error(
    ///         petsc.world(),
    ///         ErrorKind::PETSC_ERR_WRONG_MPI_SIZE,
    ///         "This is a uniprocessor example only!",
    ///     )?;
    /// }
    /// let mut v = petsc.vec_create()?;
    /// v.set_sizes(None, 10)?; // create vector of size 10
    /// v.set_from_options()?;
    /// v.set_all(Scalar::from(1.5))?;
    ///
    /// {
    ///     let mut v_view = v.view_mut()?;
    ///     assert_eq!(v_view.as_slice().unwrap(), &[Scalar::from(1.5); 10]);
    ///     v_view[2] = Scalar::from(9.0);
    /// }
    ///
    /// let v_view = v.view()?;
    /// // We do the map in the case that `Scalar` is complex.
    /// assert_eq!(
    ///     v_view.as_slice().unwrap(),
    ///     &[1.5, 1.5, 9.0, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5]
    ///         .iter()
    ///         .cloned()
    ///         .map(|v| Scalar::from(v))
    ///         .collect::<Vec<_>>()[..]
    /// );
    /// # Ok(())
    /// # }
    /// ```
    pub fn view_mut<'b>(&'b mut self) -> crate::Result<VecViewMut<'a, 'b>> {
        VecViewMut::new(self)
    }

    /// Create a default Vec from a slice
    ///
    /// Note, [`set_from_options()`](Vec::set_from_options()) will be used while constructing the vector.
    ///
    /// # Parameters
    ///
    /// * `world` - the comm world for the vector
    /// * `slice` - values to initialize this processor's portion of the vector data with
    ///
    /// The resulting vector will have the local size be set to `slice.len()`
    ///
    /// # Example
    ///
    /// ```
    /// # use petsc::{mpi::traits::*, prelude::*, Scalar};
    /// # fn main() -> petsc::Result<()> {
    /// # let petsc = petsc::Petsc::init_no_args()?;
    /// // note, cargo wont run tests with mpi so this will always be run with
    /// // a single processor, but this example will also work in a multiprocessor
    /// // comm world.
    ///
    /// // We do the map in the case that `Scalar` is complex.
    /// let values = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0]
    ///     .iter()
    ///     .cloned()
    ///     .map(|v| Scalar::from(v))
    ///     .collect::<Vec<_>>();
    /// // Will set each processor's portion of the vector to `values`
    /// let v = petsc::Vec::from_slice(petsc.world(), &values)?;
    ///
    /// let v_view = v.view()?;
    /// assert_eq!(v.local_size()?, values.len());
    /// assert_eq!(v_view.as_slice().unwrap(), &values);
    /// # Ok(())
    /// # }
    /// ```
    pub fn from_slice(world: &'a SimpleCommunicator, slice: &[Scalar]) -> crate::Result<Vec<'a>> {
        let mut v = Vec::create(world)?;
        v.set_sizes(slice.len(), None)?; // create vector of size 10
        v.set_from_options()?;
        let ix = v.ownership_range()?.collect::<std::vec::Vec<_>>();
        v.set_values(&ix, slice, InsertMode::INSERT_VALUES)?;

        v.assembly_begin()?;
        v.assembly_end()?;

        Ok(v)
    }

    /// Determines whether a PETSc [`Vec`] is of a particular type.
    pub fn type_compare(&self, type_kind: VecType) -> crate::Result<bool> {
        self.type_compare_str(&type_kind.to_string())
    }

    /// Gets the [`Vec`] type name (as a [`String`]).
    pub fn type_str(&self) -> crate::Result<String> {
        let mut s_ptr = MaybeUninit::uninit();
        let ierr = unsafe { petsc_raw::VecGetType(self.ptr, s_ptr.as_mut_ptr()) };
        self.check_error(ierr)?;

        let c_str: &CStr = unsafe { CStr::from_ptr(s_ptr.assume_init()) };
        let str_slice: &str = c_str.to_str().unwrap();
        Ok(str_slice.to_owned())
    }
}

impl<'a> Clone for Vec<'a> {
    /// Will use [`Vec::duplicate()`] and [`Vec::copy_data_from()`].
    ///
    /// Will [`petsc_panic!`] on error.
    fn clone(&self) -> Vec<'a> {
        let mut new_vec = self.duplicate().expect("failed to duplicate Vec");
        // TODO: only copy data if data has been set. It is hard to tell if data has been set.
        // We could maybe use something like `PetscObjectStateGet` to check if the vec has been modified
        // it seems like this is only incremented when data is changed so it could work. The problem is
        // that this is hidden in a private header so we can use it with the way we create raw bindings.
        new_vec.copy_data_from(self).expect("failed to copy data");
        new_vec
    }
}

impl Drop for VecViewMut<'_, '_> {
    fn drop(&mut self) {
        let ierr = unsafe {
            petsc_raw::VecRestoreArray(self.vec.ptr, &mut self.array as *mut _ as *mut _)
        };
        let _ = self.vec.check_error(ierr); // TODO: should I unwrap or what idk?
    }
}

impl Drop for VecView<'_, '_> {
    fn drop(&mut self) {
        let ierr = unsafe {
            petsc_raw::VecRestoreArray(self.vec.ptr, &mut self.array as *mut _ as *mut _)
        };
        let _ = self.vec.check_error(ierr); // TODO: should I unwrap or what idk?
    }
}

impl<'a, 'b> VecViewMut<'a, 'b> {
    /// Constructs a VecViewMut from a Vec reference
    fn new(vec: &'b mut Vec<'a>) -> crate::Result<Self> {
        let mut array = MaybeUninit::<*mut Scalar>::uninit();
        let ierr = unsafe { petsc_raw::VecGetArray(vec.ptr, array.as_mut_ptr()) };
        vec.check_error(ierr)?;

        let ndarray = unsafe {
            ArrayViewMut::from_shape_ptr(
                ndarray::IxDyn(&[vec.local_size().unwrap()]),
                array.assume_init(),
            )
        };

        Ok(Self {
            vec,
            array: unsafe { array.assume_init() },
            ndarray,
        })
    }
}

impl<'a, 'b> VecView<'a, 'b> {
    /// Constructs a VecViewMut from a Vec reference
    fn new(vec: &'b Vec<'a>) -> crate::Result<Self> {
        let mut array = MaybeUninit::<*const Scalar>::uninit();
        let ierr = unsafe { petsc_raw::VecGetArrayRead(vec.ptr, array.as_mut_ptr()) };
        vec.check_error(ierr)?;

        let ndarray = unsafe {
            ArrayView::from_shape_ptr(
                ndarray::IxDyn(&[vec.local_size().unwrap()]),
                array.assume_init(),
            )
        };

        Ok(Self {
            vec,
            array: unsafe { array.assume_init() },
            ndarray,
        })
    }
}

impl<'b> Deref for VecViewMut<'_, 'b> {
    type Target = ArrayViewMut<'b, Scalar, ndarray::IxDyn>;
    fn deref(&self) -> &ArrayViewMut<'b, Scalar, ndarray::IxDyn> {
        &self.ndarray
    }
}

impl<'b> DerefMut for VecViewMut<'_, 'b> {
    fn deref_mut(&mut self) -> &mut ArrayViewMut<'b, Scalar, ndarray::IxDyn> {
        &mut self.ndarray
    }
}

impl std::fmt::Debug for VecViewMut<'_, '_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.ndarray.fmt(f)
    }
}

impl<'b> Deref for VecView<'_, 'b> {
    type Target = ArrayView<'b, Scalar, ndarray::IxDyn>;
    fn deref(&self) -> &ArrayView<'b, Scalar, ndarray::IxDyn> {
        &self.ndarray
    }
}

impl std::fmt::Debug for VecView<'_, '_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.ndarray.fmt(f)
    }
}

impl<'a, 'bv> BorrowVec<'a, 'bv> {
    #[allow(dead_code)]
    pub(crate) fn new(
        owned_vec: ManuallyDrop<Vec<'a>>,
        drop_func: Option<Box<dyn FnOnce(&mut BorrowVec<'a, 'bv>) + 'bv>>,
    ) -> Self {
        BorrowVec {
            owned_vec,
            drop_func,
            _phantom: PhantomData,
        }
    }
}

impl<'a, 'bv> BorrowVecMut<'a, 'bv> {
    #[allow(dead_code)]
    pub(crate) fn new(
        owned_vec: ManuallyDrop<Vec<'a>>,
        drop_func: Option<Box<dyn FnOnce(&mut BorrowVecMut<'a, 'bv>) + 'bv>>,
    ) -> Self {
        BorrowVecMut {
            owned_vec,
            drop_func,
            _phantom: PhantomData,
        }
    }
}

impl<'a> Deref for BorrowVec<'a, '_> {
    type Target = Vec<'a>;

    fn deref(&self) -> &Vec<'a> {
        self.owned_vec.deref()
    }
}

impl<'a> Deref for BorrowVecMut<'a, '_> {
    type Target = Vec<'a>;

    fn deref(&self) -> &Vec<'a> {
        self.owned_vec.deref()
    }
}

impl<'a> DerefMut for BorrowVecMut<'a, '_> {
    fn deref_mut(&mut self) -> &mut Vec<'a> {
        self.owned_vec.deref_mut()
    }
}

// macro impls
impl<'a> Vec<'a> {
    wrap_simple_petsc_member_funcs! {
        VecSetFromOptions, pub set_from_options, takes mut, #[doc = "Configures the vector from the options database."];
        VecSetUp, pub set_up, takes mut, #[doc = "Sets up the internal vector data structures for the later use."];
        VecAssemblyBegin, pub assembly_begin, takes mut, #[doc = "Begins assembling the vector. This routine should be called after completing all calls to VecSetValues()."];
        VecAssemblyEnd, pub assembly_end, takes mut, #[doc = "Completes assembling the vector. This routine should be called after VecAssemblyBegin()."];
        VecSet, pub set_all, input Scalar, alpha, takes mut, #[doc = "Sets all components of a vector to a single scalar value.\n\nYou CANNOT call this after you have called [`Vec::set_values()`]."];
        VecGetLocalSize, pub local_size, output usize, ls, #[doc = "Returns the number of elements of the vector stored in local memory."];
        VecGetSize, pub global_size, output usize, gs, #[doc = "Returns the global number of elements of the vector."];
        VecNorm, pub norm, input crate::NormType, norm_type, output Real, tmp1, #[doc = "Computes the vector norm."];
        VecScale, pub scale, input Scalar, alpha, takes mut, #[doc = "Scales a vector (`x[i] *= alpha` for each `i`)."];
        VecAXPY, pub axpy, input Scalar, alpha, input &Vec, other.as_raw, takes mut, #[doc = "Computes `self += alpha * other`."];
        VecAYPX, pub aypx, input Scalar, alpha, input &Vec, other.as_raw, takes mut, #[doc = "Computes `self = other + alpha * self`."];
        VecAXPBY, pub axpby, input Scalar, alpha, input Scalar, beta, input &Vec, other.as_raw, takes mut, #[doc = "Computes `self = alpha * other + beta * self`."];
        VecAXPBYPCZ, pub axpbypcz, input Scalar, alpha, input Scalar, beta, input Scalar, gamma, input &Vec, x.as_raw, input &Vec, y.as_raw, takes mut, #[doc = "Computes `self = alpha * x + beta * y + gamma * self`."];
        VecDot, pub dot, input &Vec, y.as_raw, output Scalar, res, #[doc = "Computes the vector dot product.\n\n\
            Note, for complex vectors it does `val = (x,y) = y^H x` where `y^H` denotes the conjugate transpose of y.\
            If you with to force using the transpose you should use [`dot_t`](Vec::dot_t)."];
        VecTDot, pub dot_t, input &Vec, y.as_raw, output Scalar, res, #[doc = "Computes an indefinite vector dot product.\n\n\
            That is, as opposed to [`dot`](Vec::dot), this routine does NOT use the complex conjugate."];
        VecSetOption, pub set_option, input VecOption, option, input bool, flg, #[doc = "Sets an option for controlling a [`Vec`]'s behavior."];
        VecFilter, pub chop, input Real, tol, takes mut, #[doc = "Set all values in the vector with an absolute value less than the tolerance to zero"];
    }
}

impl_petsc_object_traits! { Vec, ptr, petsc_raw::_p_Vec, VecView, VecDestroy; }
